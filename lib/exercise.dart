import 'dart:html';
import 'dart:svg';

import 'package:logging/logging.dart';
import 'package:quiver/collection.dart';
import 'package:quiver/core.dart';

/// This defines the semitons for any given scale degree in the diatonic scales
const _semitones = const {
  1: 0, // Tonic
  2: 2, // Supertonic
  3: 4, // Mediant
  4: 5, // Subdominant
  5: 7, // Dominant
  6: 9, // Submediant
  7: 11, // Leading tone
};

var log = new Logger('Exercise');

class Exercise {
  static var fifth = new Exercise.fromDegrees('Fifth', '1 5');
  static var triad = new Exercise.fromDegrees('Triad', '1 3 5 3 1');
  static var birdy = new Exercise.fromDegrees('Birdy', '1 5 3 8 5 3 1');
  static var gamme = new Exercise.fromDegrees('Gamme', '1 3 5 8 5 3 1');
  static var gu = new Exercise.fromDegrees('Gu', '8 5 3 1 3 5 8 5 3 1', time: 3);

  static List<Exercise> get all => [fifth, triad, birdy, gamme, gu];

  /// Accepts a string of scale degrees and parses that.
  /// Example:
  ///
  ///     new Exercise.fromDegrees('triade', '1 3 5 3 1'); // Major triade
  factory Exercise.fromDegrees(String name, String exercise, {time: 4}) {
    if (exercise?.isEmpty ?? true) throw new ArgumentError('No exercise provided');

    try {
      var scaleDegrees = exercise.split(' ');

      var notes = scaleDegrees.map((degreeString) => new Note.fromDegree(degreeString));

      return new Exercise(name, notes.toList(growable: false), time: time);
    } catch (e) {
      throw new ArgumentError(e.toString());
    }
  }

  final String name;

  String get urlName => name.toLowerCase().replaceAll(' ', '-');

  final int time;

  String get id => name.toLowerCase().replaceAll(' ', '-');

  final List<Note> notes;

  Exercise(this.name, this.notes, {this.time: 4}) {
    log.finer('Creating exerice "$name" with notes: $notes');
  }

  Exercise.fromJson(Map json)
      : name = json['name'],
        time = json['time'],
        notes = (json['notes'] as List<Map>).map((noteJson) => new Note.fromJson(noteJson)).toList();

  String get imageXml =>
      getImage().outerHtml.replaceAll('<', '%3C').replaceAll('>', '%3E').replaceAll('#', '%23').replaceAll('"', "'");

  /// Generates an SVG image for this exercise
  SvgElement getImage() {
    var width = 80, height = 44;

    var lineOffset = 10.5,
        lineDistance = (height - lineOffset * 2) / 4,
        noteOffset = 15,
        noteDistance = notes.length == 1 ? 0 : (width - noteOffset * 2) / (notes.length - 1),
        ellipseWidth = width / 40,
        ellipseHeight = ellipseWidth; // / 1.5;

    Element svg = document.createElementNS("http://www.w3.org/2000/svg", "svg")
      ..setAttribute('xmlns', 'http://www.w3.org/2000/svg')
      ..setAttribute('viewPort', '0 0 $width $height')
      ..setAttribute('width', '$width')
      ..setAttribute('height', '$height');

    var svgNS = svg.namespaceUri;

    var symbol = document.createElementNS(svgNS, 'g');
    symbol.setAttribute('id', id);

    // Draw the lines
    for (var i = 0; i < 5; i++) {
      var lineY = lineOffset + lineDistance * i;
      var line = document.createElementNS(svgNS, 'line')
        ..setAttribute('stroke', 'rgba(0, 0, 0, 0.1)')
        ..setAttribute('stroke-width', '0.5')
        ..setAttribute('x1', '0')
        ..setAttribute('y1', '$lineY')
        ..setAttribute('x2', '$width')
        ..setAttribute('y2', '$lineY');
      symbol.append(line);
    }

    // Draw the notes
    for (var i = 0; i < notes.length; i++) {
      var note = notes[i];
      var noteY = height - (lineOffset + (note.degree + (note.octaves * 7)) * lineDistance / 2),
          noteX = noteOffset + noteDistance * i;
      var ellipse = document.createElementNS(svgNS, 'ellipse')
        ..setAttribute('stroke', 'rgba(0, 0, 0, 1)')
        ..setAttribute('stroke-width', '1')
        ..setAttribute('fill-opacity', '1')
        ..setAttribute('cx', '$noteX')
        ..setAttribute('cy', '$noteY')
        ..setAttribute('rx', '$ellipseWidth')
        ..setAttribute('ry', '$ellipseHeight');
      symbol.append(ellipse);
    }

    svg.append(symbol);

    return svg;
  }

  String toString() => 'Exercise "$name" with ${notes.length} notes';

  Map toJson() => {'name': name, 'time': time, 'notes': notes.map((note) => note.toJson()).toList()};

  bool operator ==(o) => o is Exercise && o.time == time && listsEqual(o.notes, notes);
  int get hashCode => hashObjects(notes.map((note) => note.hashCode).toList()..add(time.hashCode));
}

class Note {
  /// The degree in the tonic (tonic, dominant, etc...)
  int degree;

  /// How many octaves this degree spans
  int octaves;

  Accidental accidental;

  /// The length of the note in quarter notes
  int length;

  /// The interval from the tonic in semitones.
  int get interval {
    var interval = _semitones[degree];

    if (accidental == Accidental.flat) interval -= 1;
    if (accidental == Accidental.sharp) interval += 1;

    // Add the necessary semitones for given octave
    interval += octaves * 12;
    return interval;
  }

  Note({this.degree, this.octaves, this.accidental, this.length});

  Note.fromDegree(String degreeString, {this.length: 1}) {
    var match = new RegExp(r'^(\d+)(b|\#)?$').firstMatch(degreeString);
    degree = int.parse(match[1]);

    octaves = ((degree - 1) / 7).floor();
    if (octaves > 0) {
      // This exercise includes octaves
      degree -= 7 * octaves;
    }

    if (match[2] != null) {
      accidental = match[2] == 'b' ? Accidental.flat : Accidental.sharp;
    }
  }

  Note.fromJson(Map json) {
    degree = json['d'];
    octaves = json.containsKey('o') ? json['o'] : 0;
    accidental = json.containsKey('a') ? (json['a'] == -1 ? Accidental.flat : Accidental.sharp) : null;
    length = json.containsKey('l') ? json['l'] : 1;
  }

  String toString() => 'Note: ${''.padLeft(length, '♩')} $interval semitones';

  Map toJson() {
    final map = {'d': degree};
    if (octaves != 0) map['o'] = octaves;
    if (accidental != null) map['a'] = accidental == Accidental.flat ? -1 : 1;
    if (length != null && length != 1) map['l'] = length;

    return map;
  }

  bool operator ==(o) =>
      o is Note && o.degree == degree && o.octaves == octaves && o.accidental == accidental && o.length == length;
  int get hashCode => hashObjects([degree.hashCode, octaves.hashCode, accidental.hashCode, length.hashCode]);
}

enum Accidental { flat, sharp }
