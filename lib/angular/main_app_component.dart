// Copyright (c) 2017, enyo. All rights reserved. Use of this source code
// is governed by a BSD-style license that can be found in the LICENSE file.

import 'package:angular2/core.dart';
import 'package:angular2/router.dart';
import 'package:vocal_coach/angular/services/playback_service.dart';

import 'components/exercises/exercises_component.dart';
import 'services/exercises_service.dart';

@Component(
  selector: 'main-app',
  template: r'<router-outlet></router-outlet>',
  styles: const [
    '''
    :host {
      display: block;
      height: 100%;
    }
  '''
  ],
  directives: const [
    ROUTER_DIRECTIVES,
  ],
  providers: const [
    PlaybackService,
    ExercisesService,
  ],
)
@RouteConfig(const [
  const Route(path: '/exercise/...', name: 'Exercises', component: ExercisesComponent, useAsDefault: true),
])
class MainAppComponent implements OnInit {
  final ExercisesService _exercisesService;

  MainAppComponent(this._exercisesService);

  @override
  ngOnInit() {
    if (Uri.base.queryParameters.containsKey('exercises')) {
      _exercisesService.loadFromUri(Uri.base);
    }
  }
}
