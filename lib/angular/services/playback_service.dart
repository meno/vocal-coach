import 'dart:async';
import 'dart:web_audio';

import 'package:angular2/angular2.dart';
import 'package:logging/logging.dart';
import 'package:vocal_coach/exercise.dart';
import 'package:vocal_coach/utils.dart';

// create web audio api context
AudioContext audioCtx = new AudioContext();

@Injectable()
class PlaybackService {
  final log = new Logger('PlaybackService');

  /// Whether something is currently playing
  var isPlaying = false;

  /// List of notes that are still scheduled to play
  final List<Timer> _scheduledNotes = [];

  int a4 = 440;

  /// ms for attack
  int attack = 40;

  /// ms for decay
  int decay = 250;

  /// Defining the semitones relative to a4 to start the exercise from
  int rootInterval = -12;

  Future<Null> play(Exercise exercise, {bpm: 300, playPreview: false, exerciseInterval: 0}) async {
    if (isPlaying) return null;

    log.info('Playing $exercise');
    isPlaying = true;

    var noteDuration = 1 / (bpm / 60);

    var notes = new List<Note>.from(exercise.notes);

    // TODO: improve this so we simply add a note to the list with double the length
    if (playPreview) {
      var firstNote = notes.first;
      notes.insert(
          0,
          new Note(
              degree: firstNote.degree,
              octaves: firstNote.octaves,
              accidental: firstNote.accidental,
              length: exercise.time));
    }

    var getLengthForNotes = (List<Note> notes) => notes.fold(0, (prevValue, note) => prevValue + note.length);

    notes.asMap().forEach((index, note) {
      var previousLengths = getLengthForNotes(notes.sublist(0, index));
      var noteTime = (previousLengths * noteDuration * 1000).round();
      _scheduledNotes
          .add(new Timer(new Duration(milliseconds: noteTime), () => _playNote(note, exerciseInterval, bpm)));
    });

    var completer = new Completer();
    _scheduledNotes
        .add(new Timer(new Duration(milliseconds: (1000 * (getLengthForNotes(notes) + 1) * noteDuration).round()), () {
      stop();

      completer.complete();
    }));
    return completer.future;
  }

  _playNote(Note note, int exerciseInterval, int bpm) {
    var gain = audioCtx.createGain();
    gain.connectNode(audioCtx.destination);

    // Create envelope
    gain.gain.setValueAtTime(0, audioCtx.currentTime);
    gain.gain.linearRampToValueAtTime(1, audioCtx.currentTime + attack / 1000);
    gain.gain.linearRampToValueAtTime(0, audioCtx.currentTime + decay / 1000);

    var oscillator = audioCtx.createOscillator();
    oscillator.type = 'sine';
    oscillator.frequency.value = centsToHz((note.interval + rootInterval + exerciseInterval) * 100, a4);
    oscillator.connectNode(gain);

    var noteLength = 1 / (bpm / 60);

    oscillator.start(0);
    new Timer(new Duration(milliseconds: (noteLength * 1000 + decay).round()), () {
      oscillator.stop(0);
      oscillator.disconnect(0);
      gain.disconnect(0);
    });
  }

  stop() {
    log.info('Stopping playback');
    _scheduledNotes.forEach((timer) => timer.cancel());
    _scheduledNotes.clear();
    isPlaying = false;
  }
}
