import 'dart:async';
import 'dart:convert';
import 'dart:html';
import 'package:angular2/angular2.dart';
import 'package:vocal_coach/exercise.dart';

@Injectable()
class ExercisesService {
  List<Exercise> _exercises;

  Stream onExercisesChanged;

  final _onExercisesChangedController = new StreamController.broadcast();

  /// Whether exercises have been loaded from storage or are the default demo exercises
  var _isDemoExerciseList = false;

  ExercisesService() {
    onExercisesChanged = _onExercisesChangedController.stream;
    if (window.localStorage.containsKey('exercises')) {
      _exercises = decodeExercises(window.localStorage['exercises']);
    } else {
      _exercises = new List.from(Exercise.all);
      _isDemoExerciseList = true;
    }
  }

  loadFromUri(Uri uri) {
    var json = UTF8.decode(BASE64URL.decode(uri.queryParameters['exercises']));
    final exercises = decodeExercises(json);
    if (_isDemoExerciseList) {
      _exercises = exercises;
      _isDemoExerciseList = false;
    } else {
      exercises.forEach((exercise) {
        if (!_exercises.contains(exercise)) {
          _exercises.add(exercise);
        }
      });
    }
  }

  List<Exercise> get exercises {
    return _exercises;
  }

  addExercise(Exercise exercise) {
    _exercises.add(exercise);
    _onExercisesChangedController.add(null);
    window.localStorage['exercises'] = encodeExercises(_exercises);
  }

  removeExercise(Exercise exercise) {
    _exercises.remove(exercise);
    _onExercisesChangedController.add(null);
    window.localStorage['exercises'] = encodeExercises(_exercises);
  }

  static String encodeExercises(List<Exercise> exercises) =>
      JSON.encode(exercises.map((exercise) => exercise.toJson()).toList());

  static List<Exercise> decodeExercises(String json) =>
      (JSON.decode(json) as List<Map>).map((exerciseJson) => new Exercise.fromJson(exerciseJson)).toList();
}
