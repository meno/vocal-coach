import 'package:angular2/core.dart';
import 'package:vocal_coach/angular/services/exercises_service.dart';
import 'package:vocal_coach/exercise.dart';

@Component(
  selector: 'vc-new-exercise',
  styleUrls: const ['new_exercise_component.css'],
  templateUrl: 'new_exercise_component.html',
  providers: const [],
)
class NewExerciseComponent {
  final ExercisesService _exercisesService;
  String name = '';

  String notes = '';

  NewExerciseComponent(this._exercisesService);

  createExercise() {
    _exercisesService.addExercise(new Exercise.fromDegrees(name, notes));
  }
}
