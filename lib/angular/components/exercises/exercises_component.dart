import 'dart:async';
import 'dart:convert';
import 'dart:html';

import 'package:angular2/core.dart';
import 'package:angular2/router.dart';
import 'package:angular2/src/security/dom_sanitization_service.dart';
import 'package:vocal_coach/angular/services/exercises_service.dart';
import 'package:vocal_coach/exercise.dart';

import '../exercise/exercise_component.dart';
import '../new_exercise/new_exercise_component.dart';
import '../select_exercise/select_exercise_component.dart';

@Component(
  selector: 'vc-exercises',
  styleUrls: const ['exercises_component.css'],
  templateUrl: 'exercises_component.html',
  directives: const [
    ROUTER_DIRECTIVES,
  ],
  providers: const [
    CurrentExerciseService,
  ],
)
@RouteConfig(const [
  const Route(path: '/', name: 'Select', component: SelectExerciseComponent, useAsDefault: true),
  const Route(path: '/new', name: 'NewExercise', component: NewExerciseComponent),
  const Route(path: '/:exerciseName', name: 'Exercise', component: ExerciseComponent),
])
class ExercisesComponent implements OnInit, OnDestroy {
  List<Exercise> exercises;
  final ExercisesService _exercisesService;
  final CurrentExerciseService _currentExerciseService;
  final Router _router;
  final DomSanitizationService domSanitizationService;

  ExercisesComponent(this._exercisesService, this._router, this._currentExerciseService, this.domSanitizationService);

  String bookmarkLink;

  List<StreamSubscription> _streamSubscriptions;

  @override
  ngOnInit() {
    _setBookmarkLink() => bookmarkLink =
        './?exercises=${BASE64URL.encode(UTF8.encode(ExercisesService.encodeExercises(_exercisesService.exercises)))}';
    exercises = _exercisesService.exercises;
    _setBookmarkLink();
    _streamSubscriptions = [
      _exercisesService.onExercisesChanged.listen((_) {
        exercises = _exercisesService.exercises;
        _setBookmarkLink();
      }),
      document.body.onKeyDown.listen((event) {
        var acted = false;
        switch (event.keyCode) {
          case KeyCode.LEFT:
            if (_currentExerciseService.currentExercise != null) {
              for (var i = exercises.length - 1; i >= 1; i--) {
                if (exercises[i] == _currentExerciseService.currentExercise) {
                  _router.navigate([
                    'Exercise',
                    {'exerciseName': exercises[i - 1].urlName}
                  ]);
                  break;
                }
              }
            }
            break;
          case KeyCode.RIGHT:
            if (_currentExerciseService.currentExercise != null) {
              for (var i = 0; i < exercises.length - 1; i++) {
                if (exercises[i] == _currentExerciseService.currentExercise) {
                  _router.navigate([
                    'Exercise',
                    {'exerciseName': exercises[i + 1].urlName}
                  ]);
                  break;
                }
              }
            }
            break;
        }
      })
    ];
  }

  @override
  ngOnDestroy() {
    _streamSubscriptions?.forEach((subscription) => subscription.cancel());
  }
}

@Injectable()
class CurrentExerciseService {
  Exercise currentExercise;
}
