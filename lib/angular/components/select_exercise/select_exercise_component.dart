import 'package:angular2/core.dart';

@Component(
  selector: 'vc-select-exercise',
  template: 'Select an exercise',
  styles: const [
    '''
    :host {
      display: flex;
      align-items: center;
      justify-content: center;
      height: 100%;
    }
    '''
  ],
)
class SelectExerciseComponent {}
