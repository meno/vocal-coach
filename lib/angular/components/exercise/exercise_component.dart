import 'dart:async';
import 'dart:html';

import 'package:angular2/core.dart';
import 'package:angular2/router.dart';
import 'package:vocal_coach/angular/services/exercises_service.dart';
import 'package:vocal_coach/angular/services/playback_service.dart';
import 'package:vocal_coach/exercise.dart';
import 'package:vocal_coach/utils.dart';

import '../exercises/exercises_component.dart';

@Component(
  selector: 'vc-exercise',
  styleUrls: const ['exercise_component.css'],
  templateUrl: 'exercise_component.html',
  directives: const [],
  providers: const [],
)
class ExerciseComponent implements OnInit, OnDestroy {
  final RouteParams _routeParams;
  final PlaybackService _playbackService;
  final ExercisesService _exercisesService;
  final CurrentExerciseService _currentExerciseService;
  final Router _router;

  Exercise exercise;

  String exerciseNote;

  bool isPlaying = false;

  bool isAscending = true;

  bool isContinuous = false;

  bool playPreview = false;

  ExerciseComponent(
    this._routeParams,
    this._playbackService,
    this._exercisesService,
    this._currentExerciseService,
    this._router,
  );

  int _bpm = 300;

  int get bpm => _bpm;

  set bpm(dynamic bpm) {
    _bpm = bpm is int ? bpm : int.parse(bpm);
  }

  int _exerciseInterval;

  int get exerciseInterval => _exerciseInterval;

  /// Whether this exercise has already played at least once.
  /// This is to make sure that when hitting the "Next" button, we don't increase the scale.
  var _alreadyPlayed = false;

  @Input()
  set exerciseInterval(int exerciseInterval) {
    _exerciseInterval = exerciseInterval;
    exerciseNote = noteNameFromInterval(exerciseInterval + _playbackService.rootInterval);
  }

  List<StreamSubscription> _streamSubscriptions;

  @override
  ngOnInit() async {
    exerciseInterval = 0;
    exercise = _exercisesService.exercises
        .firstWhere((exercise) => exercise.urlName == _routeParams.get('exerciseName'), orElse: () => null);
    if (exercise == null) {
      await _router.navigate(['Select']);
      return;
    }
    _currentExerciseService.currentExercise = exercise;
    _streamSubscriptions = [
      document.body.onKeyDown.listen((event) {
        var acted = false;
        switch (event.keyCode) {
          case KeyCode.SPACE:
            if (isPlaying && isContinuous) {
              // Setting _alreadyPlayed to false, so when resuming the exercise it's on the right scale
              _alreadyPlayed = false;
              stop();
            } else
              next();
            // flashFocus($['play-next-button']);
            acted = true;
            break;
          case KeyCode.ENTER:
            if (isPlaying && isContinuous) {
              // Setting _alreadyPlayed to false, so when resuming the exercise it's on the right scale
              _alreadyPlayed = false;
              stop();
            } else
              play();
            // flashFocus($['play-button']);
            acted = true;
            break;
          case KeyCode.ESC:
            if (isPlaying) {
              _alreadyPlayed = false;
              stop();
            } else {
              reset();
            }
            // flashFocus($['reset-button']);
            acted = true;
            break;
          case KeyCode.P:
            playPreview = !playPreview;
            acted = true;
            break;
          case KeyCode.A:
            isAscending = !isAscending;
            acted = true;
            break;
          case KeyCode.C:
            isContinuous = !isContinuous;
            acted = true;
            break;
          case KeyCode.DOWN:
            moveDown();
            // flashFocus($['move-down-button']);
            acted = true;
            break;
          case KeyCode.UP:
            moveUp();
            // flashFocus($['move-up-button']);
            acted = true;
            break;
          case KeyCode.NUM_PLUS:
            bpm += 10;
            break;
          case KeyCode.NUM_MINUS:
            bpm -= 10;
            break;
        }
        if (acted) {
          event.preventDefault();
        }
      })
    ];
  }

  @override
  ngOnDestroy() {
    stop();
    _streamSubscriptions?.forEach((subscription) => subscription.cancel());
  }

  play() async {
    _alreadyPlayed = true;
    isPlaying = true;
    await _playbackService.play(exercise, bpm: bpm, exerciseInterval: exerciseInterval, playPreview: playPreview);
    print('$isContinuous $isPlaying');
    if (isContinuous && isPlaying) {
      next();
    } else {
      isPlaying = false;
    }
  }

  next() {
    if (isPlaying) stop();
    if (_alreadyPlayed) {
      // We don't want to increase / decrease the scale, if it's
      // the first time this exercise is beeing played.
      if (isAscending) {
        exerciseInterval++;
      } else {
        exerciseInterval--;
      }
    }
    play();
  }

  stop() {
    _playbackService.stop();
    isPlaying = false;
  }

  moveUp() {
    _alreadyPlayed = false;
    exerciseInterval++;
  }

  moveDown() {
    _alreadyPlayed = false;
    exerciseInterval--;
  }

  reset() {
    stop();
    _alreadyPlayed = false;
    exerciseInterval = 0;
  }

  remove() {
    _exercisesService.removeExercise(exercise);
    _router.navigate(['/Exercises']);
  }
}
