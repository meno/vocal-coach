// Copyright (c) 2017, enyo. All rights reserved. Use of this source code
// is governed by a BSD-style license that can be found in the LICENSE file.

import 'package:angular2/angular2.dart';
import 'package:angular2/platform/browser.dart';
import 'package:angular2/platform/common.dart';
import 'package:angular2/router.dart';
import 'package:vocal_coach/angular/main_app_component.dart';

main() {
  bootstrap(MainAppComponent, [ROUTER_PROVIDERS, const Provider(LocationStrategy, useClass: HashLocationStrategy)]);
}
