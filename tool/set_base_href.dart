import 'dart:io';

main() {
  var file = new File('build/web/index.html');
  var content = file.readAsStringSync();
  content = content.replaceFirst('<base href="/">', '<base href="/vocal-coach/">');
  file.writeAsStringSync(content);
}
